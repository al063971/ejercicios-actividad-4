package actividad4;

import java.util.Arrays;
public class Program {
    public static String AlphabetSoup(String s) {
      char[] chars = s.toCharArray();
      Arrays.sort(chars);
      return new String(chars);
    }
}
